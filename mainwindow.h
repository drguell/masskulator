#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QFile>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QFileInfo>
#include <QDir>
#include <QDateTime>
#include <QString>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QtCharts>
#include <QChartView>
#include <QLineSeries>
#include <QtMath>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QSqlDatabase db;
    QString databaseFilename = "/.local/share/masskulator/masskulator.db";

    double firstWeight;
    double lastWeight;
    double lastlastWeight;
    double minWeight;
    double maxWeight;

    QChart *chartLine1;
    QChartView *chartViewLine2;
    QLineSeries *seriesLine2;
    QDateTimeAxis *axisX11;
    QDateTimeAxis *axisX21;
    QValueAxis *axisY1;

    QChart *chartLine2;
    QChartView *chartViewLine1;
    QLineSeries *seriesLine1;
    QDateTimeAxis *axisX12;
    QDateTimeAxis *axisX22;
    QValueAxis *axisY2;

    void printGraphLastnDays(int days, QFrame *graphFrame);

    int totalDays = 0;

    void addMenus();
    void createLocalShareDirectory(QString dbDirectory);
    void initializeDb(QString dbFilename, int dbSize);
    void printTable();
    void fetchData();
    double getPreviousYearlyWeight();
    double getPreviousMonthlyWeight();

private slots:
    void quit();
    void on_pushButton_add_clicked();
    void on_pushButton_delete_clicked();

private:
    Ui::MainWindow *ui;

protected:
    void resizeEvent(QResizeEvent *event) override;

};
#endif // MAINWINDOW_H
