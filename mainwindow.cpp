#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <KTextEditor/Document>
#include <KTextEditor/Editor>
#include <KTextEditor/View>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Add menu to window
    addMenus();

    // Refer to DB file
    QFile dbfile(QDir::homePath() + databaseFilename);

    // Create directory
    createLocalShareDirectory(dbfile.fileName());

    // Initialize DB
    initializeDb(dbfile.fileName(), dbfile.size());

    // fetch data
    fetchData();

    // Creating the 2 graphs
    printGraphLastnDays(totalDays+1, ui->verticalFrame_4);

    // Print the weight table
    printTable();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::quit()
{
    QCoreApplication::quit();
}

void MainWindow::addMenus()
{
    /*
     * Generating menus
     */
    // File Menu
    QMenu *fileMenu;
    fileMenu = menuBar()->addMenu("&File");

    QAction *quit = new QAction("&Quit", this   );
    quit->setShortcut(tr("CTRL+Q"));

    connect(quit, &QAction::triggered, this, &MainWindow::quit);

    fileMenu->addAction(quit);
}

void MainWindow::on_pushButton_add_clicked()
{
    // Get value from weight input widget
    QString weightInput = ui->doubleSpinBox->text().replace(",",".");

    // Put Data in a QPointF to insert into SeriesLine
    QPointF qpointToInsert(ui->calendarWidget->selectedDate().startOfDay().toMSecsSinceEpoch(),weightInput.toDouble());

    // Checked if empty data and INSERT in db
    if (ui->doubleSpinBox->text() == "") {
        QMessageBox::warning(this, "Alerta", "Add a valid weight");
    } else {
        QFile dbfile(QDir::homePath() + databaseFilename);
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(dbfile.fileName());
        if (db.open())
        {
            QSqlQuery querySelectAllRows;
            querySelectAllRows.exec("SELECT peso, timestamp FROM weight WHERE "
                                    "timestamp = '" + ui->calendarWidget->selectedDate().toString("yyyy-MM-dd") + "'");

            if (querySelectAllRows.next()) {
                QMessageBox::warning(this, "Alerta", "Value exists for this day!");
            } else {
                QSqlQuery queryInsertNewWeight;
                queryInsertNewWeight.exec("INSERT INTO weight(peso, timestamp) "
                                          "VALUES('" + weightInput + "', '" + ui->calendarWidget->selectedDate().toString("yyyy-MM-dd") + "');");

                // rePrint table
                printTable();

                // add new value to the actual graphs
                int countSeriesLine1 = seriesLine1->count();

                for (int i=0; i<=countSeriesLine1; i++){

                    if ( ui->calendarWidget->selectedDate().startOfDay() < QDateTime::fromMSecsSinceEpoch(seriesLine1->at(i).x()) ) {
                        seriesLine1->insert(i, qpointToInsert);
                        break;
                    }

                    if ( i == countSeriesLine1-1 ) {
                        seriesLine1->append(ui->calendarWidget->selectedDate().startOfDay().toMSecsSinceEpoch(),weightInput.toDouble());
                        break;
                    }
                }

                // Search for min and max value to plot y axi correctly
                double min1 = 999.0;
                double max1 = 0.0;

                for (int i=0; i<=seriesLine1->count()-1; i++){
                    if ( seriesLine1->at(i).y() < min1 ) {
                        min1 = seriesLine1->at(i).y();
                    }

                    if ( seriesLine1->at(i).y() > max1) {
                        max1 = seriesLine1->at(i).y();
                    }
                }

                axisX11->setRange(QDateTime::fromMSecsSinceEpoch(seriesLine1->at(0).x()),
                                QDateTime::fromMSecsSinceEpoch(seriesLine1->at(seriesLine1->count()-1).x()));

                axisY1->setRange(min1, max1+1);
            }

            // Refresh global data information
            fetchData();

            db.close();
        } else {
            qDebug() << "Error opening Database";
        }
    }
}

void MainWindow::on_pushButton_delete_clicked()
{
    if ( ui->tableWidget->selectedItems().count() != 0 ) {
        double valueToDelete = ui->tableWidget->item(ui->tableWidget->currentRow(),1)->text().toDouble();

        QDateTime timeToDelete = QDateTime::fromString(ui->tableWidget->item(ui->tableWidget->currentRow(),0)->text(), "dd/MM/yyyy");

        QFile dbfile(QDir::homePath() + databaseFilename);
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(dbfile.fileName());
        if (db.open())
        {
            QSqlQuery queryInsertNewWeight;
            queryInsertNewWeight.exec("DELETE FROM weight "
                                      "WHERE timestamp='"
                                      + timeToDelete.toString("yyyy-MM-dd") + "';");
            db.close();
        } else {
            qDebug() << "Error opening Database";
        }

        printTable();

        // Deleting values from graph and reprint it
        // Search value in seriesLine1 and delete it
        int countSeriesLine1 = seriesLine1->count()-1;

        for (int i=0; i<=countSeriesLine1; i++){
            if ( QDateTime::fromMSecsSinceEpoch(seriesLine1->at(i).x()).toString("yyyy-MM-dd") == timeToDelete.toString("yyyy-MM-dd") ) {
                if (seriesLine1->at(i).y() == valueToDelete ){
                    seriesLine1->removePoints(i,1);
                }
            }
        }

        // Search for min and max value to plot y axi correctly
        double min1 = 999.0;
        double max1 = 0.0;

        for (int i=0; i<=seriesLine1->count()-1; i++){
            if ( seriesLine1->at(i).y() < min1 ) {
                min1 = seriesLine1->at(i).y();
            }

            if ( seriesLine1->at(i).y() > max1) {
                max1 = seriesLine1->at(i).y();
            }
        }

        axisX11->setRange(QDateTime::fromMSecsSinceEpoch(seriesLine1->at(0).x()),
                        QDateTime::fromMSecsSinceEpoch(seriesLine1->at(seriesLine1->count()-1).x()));

        axisY1->setRange(min1, max1+1);

        // Refresh global data
        fetchData();
    } else {
        QMessageBox::warning(this, "Alerta", "Select a valid table value.");
        qDebug() << "Select a valid table value";
    }
}

double MainWindow::getPreviousYearlyWeight()
{

    double yearlyPreviousWeight = 0.0;

    QDateTime currentDay = QDateTime::currentDateTime();
    QDateTime yearlyDaysAgo = currentDay.addDays(-360);

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDir::homePath() + databaseFilename);

    if (!db.open()){
        qDebug() << "Error opening DB";
    }

    QSqlQuery querySelectAllRows;
    querySelectAllRows.exec("SELECT peso, timestamp FROM weight ORDER BY timestamp;");

    while (querySelectAllRows.next())
    {
        QString timestamp = querySelectAllRows.value("timestamp").toString();
        QDateTime timeDate = QDateTime::fromString(timestamp, "yyyy-MM-dd");
        // QDateTime monthDate = QDateTime::fromString(timestamp, "MM");
        // QString month = monthDate.toString("MM");

        double peso = querySelectAllRows.value("peso").toDouble();


        if (timeDate >= yearlyDaysAgo && timeDate < currentDay) {
            qDebug() << "Date in of year: " << timeDate << peso;
        } else {
            qDebug() << "Date out of year: " << timeDate;
            yearlyPreviousWeight = peso;
        }
    }

    return yearlyPreviousWeight;

}

double MainWindow::getPreviousMonthlyWeight()
{
    double monthlyPreviousWeight = 0.0;

    QDateTime currentDay = QDateTime::currentDateTime();
    QDateTime monthlyDaysAgo = currentDay.addDays(-30);

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDir::homePath() + databaseFilename);

    if (!db.open()){
        qDebug() << "Error opening DB";
    }

    QSqlQuery querySelectAllRows;
    querySelectAllRows.exec("SELECT peso, timestamp FROM weight ORDER BY timestamp;");

    while (querySelectAllRows.next())
    {
        QString timestamp = querySelectAllRows.value("timestamp").toString();
        QDateTime timeDate = QDateTime::fromString(timestamp, "yyyy-MM-dd");
        QDateTime monthDate = QDateTime::fromString(timestamp, "MM");
        // QString month = monthDate.toString("MM");

        double peso = querySelectAllRows.value("peso").toDouble();


        if (timeDate >= monthlyDaysAgo && timeDate < currentDay) {
            qDebug() << "Date in of month: " << timeDate << peso;
        } else {
            qDebug() << "Date out of month: " << timeDate;
            monthlyPreviousWeight = peso;
        }

    }

    return monthlyPreviousWeight;
}

void MainWindow::fetchData()
{
    firstWeight = 0.0;
    lastlastWeight = 0.0;
    lastWeight = 0.0;
    minWeight = 999.0;
    maxWeight = 0.0;

    QDateTime firstDate = QDateTime::currentDateTime();
    QDateTime lastDate;
    int diffDate;

    // QDateTime currentDay = QDateTime::currentDateTime();

    // Buscamos el número de filas actuales en la DB
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDir::homePath() + databaseFilename);

    if (!db.open()){
        qDebug() << "Error opening DB";
    }

    QSqlQuery querySelectAllRows;
    querySelectAllRows.exec("SELECT peso, timestamp FROM weight ORDER BY timestamp;");

    while (querySelectAllRows.next())
    {
        double peso = querySelectAllRows.value("peso").toDouble();
        QString timestamp = querySelectAllRows.value("timestamp").toString();
        QDateTime timeDate = QDateTime::fromString(timestamp, "yyyy-MM-dd");
        // QDateTime monthDate = QDateTime::fromString(timestamp, "MM");
        // QString month = monthDate.toString("MM");

        if (firstDate > timeDate)
        {
            firstDate = timeDate;
            firstWeight = peso;
        }

        if (lastDate < timeDate)
        {
            lastDate = timeDate;
            lastlastWeight = lastWeight;
            lastWeight = peso;
        }

        // Search minWeight
        if ( minWeight > peso )
        {
            minWeight = peso;
        }

        // Search maxWeight
        if ( maxWeight < peso )
        {
            maxWeight = peso;
        }
    }

    diffDate = firstDate.secsTo(QDateTime::currentDateTime())/60/60/24;

    double totalDifferenceValue = qFabs(minWeight - maxWeight);
    ui->label_minWeightValue->setNum(minWeight);
    ui->label_maxWeightValue->setNum(maxWeight);
    ui->label_firstWeightValue->setNum(firstWeight);
    ui->label_lastWeightValue->setNum(lastWeight);
    ui->label_differenceValue->setNum(lastWeight-firstWeight);
    //ui->label_totalDifferenceValue->setNum(totalDifferenceValue);
    ui->label_totalDifferenceValue->setText(QStringLiteral("|%1|").arg(totalDifferenceValue ));
    ui->label_lastDiferenceValue->setNum(lastWeight-lastlastWeight);

    ui->label_firstDateValue->setText(firstDate.toString("dd/MM/yyyy"));
    ui->label_lastDateValue->setText(lastDate.toString("dd/MM/yyyy"));
    ui->label_daysDifferenceValue->setNum(diffDate);

    totalDays = diffDate;

    ui->doubleSpinBox->setValue(lastWeight);

    db.close();
    QSqlDatabase::removeDatabase("QSQLITE");

}

void MainWindow::createLocalShareDirectory(QString dbDirectory)
{
    QDir directory(dbDirectory.section("/",0,-2));

    if (!directory.exists())
    {
        if (!QDir().mkdir(directory.absolutePath()))
        {
            qDebug() << "Directory wasn't created";
        }
    }
}

void MainWindow::initializeDb(QString dbFilename, int dbSize)
{
    // Initialitzing SQLite DB
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dbFilename);

    if (!db.open())
    {
        qDebug() << "Error opening Database";
    }

    if (dbSize == 0)
    {
        qDebug() << "File not exist";
        QSqlQuery query;
        query.exec("CREATE TABLE 'weight' "
                   "('timestamp' TEXT PRIMARY KEY,"
                   "'peso' REAL NOT NULL);");
    }

    db.close();
}

void MainWindow::printGraphLastnDays(int days, QFrame *graphFrame)
{

    // Recorremos los N últimos días
    // double previousWeight = 0.0;
    QDateTime currentDay = QDateTime::currentDateTime();
    QDateTime nDaysAgo = currentDay.addDays(-days);

    // Initializing new graph
    QChart *chartLine = new QChart;

    // Search for the number of rowns in the db
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDir::homePath() + databaseFilename);

    if (!db.open()){
        qDebug() << "Error opening DB";
    }

    // Initializing new series
    QLineSeries *seriesLine = new QLineSeries();

    // Search for each day if there are a value in DB
    for (QDateTime i=nDaysAgo; i <= currentDay; i=i.addDays(+1))
    {
        QSqlQuery querySelectAllRows;
        querySelectAllRows.exec("SELECT peso, timestamp FROM weight ORDER BY timestamp;");

        while (querySelectAllRows.next())
        {
            double peso = querySelectAllRows.value("peso").toDouble();
            QString timestamp = querySelectAllRows.value("timestamp").toString();

            if (i.toString("yyyy-MM-dd") == timestamp)
            {
                seriesLine->append(i.toMSecsSinceEpoch(),peso);
                seriesLine->setPointsVisible(true);
            }
        }
    }

    db.close();

    chartLine->addSeries(seriesLine);
    chartLine->setTitle("Weight Evolution: Last " + QString::number(days) + " days (Kg)");

    // Initializing new ChartView
    QChartView *chartViewLine = new QChartView(chartLine);

    chartViewLine->setRenderHint(QPainter::Antialiasing);

    // Creating the Axis
    QDateTimeAxis *axisX = new QDateTimeAxis;
    axisX->setTickCount(10);
    axisX->setFormat("dd/MM");
    chartLine->addAxis(axisX, Qt::AlignBottom);
    seriesLine->attachAxis(axisX);

    QDateTimeAxis *axisX2 = new QDateTimeAxis;
    axisX2->setTickCount(10);
    axisX2->setFormat("yyyy");
    axisX2->setTitleText("Date");
    chartLine->addAxis(axisX2, Qt::AlignBottom);
    seriesLine->attachAxis(axisX2);

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%i");
    axisY->setTitleText("Weight Kg");

    chartLine->addAxis(axisY, Qt::AlignLeft);
    seriesLine->attachAxis(axisY);

    chartViewLine->setParent(graphFrame);

    // Needed to fit in the QFrame (graphFrame)
    chartViewLine->resize(chartViewLine->parentWidget()->size());

    // Assign all global variables
    // If we need to change it in the future
    if ( days == 60 )
    {
        chartLine2 = chartLine;
        chartViewLine2 = chartViewLine;
        seriesLine2 = seriesLine;
        axisX12 = axisX;
        axisX22 = axisX2;
        axisY2 = axisY;
    } else {
        chartLine1 = chartLine;
        chartViewLine1 = chartViewLine;
        seriesLine1 = seriesLine;
        axisX11 = axisX;
        axisX21 = axisX2;
        axisY1 = axisY;
    }
}

void MainWindow::printTable()
{
    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(0);

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QDir::homePath() + databaseFilename);

    if (!db.open()){
        qDebug() << "Error opening DB";
    }

    QSqlQuery querySelectAllRows;
    querySelectAllRows.exec("SELECT peso, timestamp FROM weight ORDER BY timestamp DESC;");

    while (querySelectAllRows.next())
    {
        QString peso = querySelectAllRows.value("peso").toString();
        QString timestamp = querySelectAllRows.value("timestamp").toString();
        QDateTime timeDate = QDateTime::fromString(timestamp, "yyyy-MM-dd");
        QDateTime monthDate = QDateTime::fromString(timestamp, "MM");
        QString month = monthDate.toString("MM");

        int tableRows = ui->tableWidget->rowCount();

        ui->tableWidget->setRowCount(tableRows+1);

        QTableWidgetItem* tableItemDate = new QTableWidgetItem;

        tableItemDate->setText(timeDate.toString("dd/MM/yyyy"));
        ui->tableWidget->setItem(ui->tableWidget->currentRow()+tableRows, 2, tableItemDate);

        QTableWidgetItem* tableItemWeight = new QTableWidgetItem;

        tableItemWeight->setText(peso);
        tableItemWeight->setTextAlignment(Qt::AlignVCenter|Qt::AlignRight);
        ui->tableWidget->setItem(ui->tableWidget->currentRow()+tableRows, 3, tableItemWeight);
    }

    db.close();

    // Stretch table to it's size
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);
    ui->tableWidget->setAlternatingRowColors(true);
}

void MainWindow::resizeEvent(QResizeEvent *event) {

    QMainWindow::resizeEvent(event);

    chartViewLine1->resize(chartViewLine1->parentWidget()->size());

    //ui->horizontalLayout_4->children().size();

}
